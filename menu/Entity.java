package menu;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Entity {
  ArrayList<Entity> children = new ArrayList<Entity>();
  Entity parent;
  ArrayList<Action> actions = new ArrayList<Action>();

  public void addAction(Action action) {
    this.actions.add(action);
  }

  public void nextAction(Entity nextTime) {
    this.children.add(nextTime);
    nextTime.parent = this;
  }

  public ArrayList<Action> getAction() {
    return this.actions;
  }

  public void setAction(Action action) {
    this.actions.add(action);
  }

  public Entity getParent() {
    return this.parent;
  }

  public void setParent(Entity parent) {
    this.parent = parent;
  }

  public ArrayList<Entity> getChildren() {
    return this.children;
  }

  public void setChildren(Entity children) {
    this.children.add(children);
    children.parent = this;
  }

  public int sizeChildren() {
    return this.children.size();
  }

  public int sizeAction() {
    return this.actions.size();
  }

  public void user() {
    System.out.println("Выберите уровень: ");
    if (this.parent == null) {
      System.out.println("0. Выход");
    }
    else {
      System.out.println("0. Назад");
    }
    int num = 1;
    for (int i=0; i<this.children.size(); i++) {
      System.out.println(num + ". " + "Следующий уровень");
      num++;
    }
    for (int i=0; i<this.actions.size(); i++) {
        System.out.println(num +". " + "Выбрать эту эпоху");
        num++;
    }
    System.out.println();
  }

}
