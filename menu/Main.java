package menu;

import java.util.Scanner;

public class Main {
  public static Entity buildMenu() {
    Entity startGame = new Entity();
    Entity levelOne = new Entity();
    Entity levelOneP2 = new Entity();
    Entity levelTwo = new Entity();
    Entity levelTwoP2 = new Entity();

    Action primitive = new Action("Выбрать эту эпоху");
    primitive.setMessage("Первобытное общество."+ "\n" +
    "История первобытного общества охватывает период с момента появления первобытного человека до образования певых государств в Азии и Африке.");

    Action ancient = new Action("Выбрать эту эпоху");
    ancient.setMessage("Древний мир." + "\n" +
     "История древнего мира изучает существование древнейших цивилизаций" + "\n" +
    "(Древний Восток, Древняя Греция, Древний Рим) с момента возникновения до V века н.э");

    Action middle = new Action("Выбрать эту эпоху");
    middle.setMessage("Средние века." + "\n" +
     "История средних веков затрагивает период с V по XVI век. Концом европейского средневековья считается начало Нидерландской буржуазной революции (1566 г.).");

    Action newTime = new Action("Выбрать эту эпоху");
    newTime.setMessage("Новое время." + "\n" +
     "Новой историей учёные считают XVI – конец XVIII века." + "\n" +
    "Хронологическим рубежом, отделяющим Новое время от последующей эпохи," + "\n" +
    "одни учёные считают начало Великой французской революции 1789 – 1799 годов," + "\n" +
    "другие – окончание первой мировой войны 1914 – 1918 годов.");

    Action modern = new Action("Выбрать эту эпоху");
    modern.setMessage("Новейшее время." + "\n" +
     "Новейшая история, по мнению одних," + "\n" +
    "охватывает период с 1789 года до окончания второй мировой войны 1939—1945 годов," + "\n" +
    "а по мнению других — с 1918 года по настоящее время.");

    startGame.nextAction(levelOne);
    startGame.nextAction(levelOneP2);
    startGame.addAction(primitive);

    levelOne.addAction(ancient);
    levelOne.nextAction(levelTwo);
    levelOne.nextAction(levelTwoP2);

    levelOneP2.addAction(middle);

    levelTwo.addAction(newTime);

    levelTwoP2.addAction(modern);
    return startGame;
  }

  public static void main(String[] args) throws Exception {
    Main m = new Main();
    Entity startGame = m.buildMenu();
    Scanner sc = new Scanner(System.in);
    int userInput;
    while (true) {
      startGame.user();
      userInput = sc.nextInt();
      System.out.println();
      if (userInput == 0) {
        if (startGame.parent == null) {
          System.out.println("Теперь вы знаете чуть больше!");
          break;
        }
        else startGame = startGame.parent;
      }

      if (userInput > startGame.sizeChildren() && userInput <= startGame.sizeAction() + startGame.sizeChildren()) {
        startGame.actions.get(userInput - startGame.sizeChildren() - 1).act();
      }
      if (userInput > 0 && userInput <= startGame.sizeChildren()) {
        startGame = startGame.children.get(userInput - 1);
      }
    }
  }
}
