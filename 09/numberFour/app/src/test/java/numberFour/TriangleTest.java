package numberFour;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class TriangleTest {
  private Triangle triangle;
  @BeforeEach
  public void nameHim() {
    triangle = newTriangle(5);
  }
  @Test void PerTest() {
    assertEquals(15, triangle.getPerimetr());
  }
  @Test void PerString() {
    assertEqual("my perimetr = 15", triangle.print());
  }
}
