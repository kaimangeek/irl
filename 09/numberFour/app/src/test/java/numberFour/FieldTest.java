package numberFour;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class FieldTest {
  private Field field;
  @BeforeEach
  public void nameHim() {
    field = new Field(5);
  }
  @Test void printField() {
    assertEquals("Im field", field.print());
  }
}
