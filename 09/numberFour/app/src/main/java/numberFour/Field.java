package numberFour;

public class Field implements Printer {
  protected int quantity;
  protected int size;
  Field(int size) {
    this.size = size;
  }
  public int getPerimetr() {
    int perimetr = this.quantity * this.size;
    return perimetr;
  }
  public String toString() {
    return "Perimetr of field = " + this.getPerimetr();
  }
  public String print() {
    return "This is field";
  }
}
