package numberOne;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ExceptTest {
  Matrix matOne = new Matrix(3, 3);
  Matrix matTwo = new Matrix(2, 3);
  Matrix noneMat = new Matrix();
  Matrix noneTwo = new Matrix();
  Operations oper = new Operations();
  @Test void SetMatrix() throws NewException {
    int[][] createMat = new int[][] {{2, 5, 7}, {1, 3, 6}, {9, 2, 4}};
    matOne.setMatrix(createMat);
    assertArrayEquals(createMat, matOne.getMatrix());
  }
  @Test void stillExcept() throws NullPointerException {
    NullPointerException exception = assertThrows(NullPointerException.class, () -> oper.still(noneMat, noneTwo));
    assertEquals("matrices empty", exception.printer());
  }
  @Test void subtractExcept() throws NullPointerException {
    NullPointerException exception = assertThrows(NullPointerException.class, () -> oper.subtract(noneMat, noneTwo));
    assertEquals("matrices empty", exception.printer());
  }
  @Test void multiplyExcept() throws NullPointerException {
    NullPointerException exception = assertThrows(NullPointerException.class, () -> oper.multiply(noneMat, noneTwo));
    assertEquals("matrices empty", exception.printer());
  }
  @Test void stillLenExcept() throws NewException {
    NewException exception = assertThrows(NewException.classs, () -> oper.still(matOne, matTwo));
    assertEquals("size is wrong", exception.printer());
  }
  @Test void subtractLenExcept() throws NewException {
    NewException exception = assertThrows(NewException.class, () -> oper.subtract(matOne, matTwo));
    assertEquals("size is wrong", exception.printer());
  }
  @Test void multiplyLenExcept() throws NewException {
    NewException exception = assertThrows(NewException.class, () -> oper.multiply(matOne, matTwo));
    assertEquals("size is wrong", exception.printer());
  }
}
