package numberOne;

public class Main {
  public static void main(String[] args) {
    Operations oper = new Operations();
    int x = 3;
    int y = 3;
    Matrix matOne = new Matrix(x, y);
    matOne.generating();
    System.out.println("Matrix 1: ");
    oper.printMatrix(matOne);
    Matrix matTwo = new Matrix(x, y);
    matTwo.generating();
    System.out.println("Matrix 2: ");
    oper.printMatrix(matTwo);
    int[][] sumMatrix = oper.still(matOne, matTwo);
    System.out.println("Summing: ");
    oper.printMatrix(sumMatrix);
    int[][] subtractMatrix = oper.subtract(matOne, matTwo);
    System.out.println("Subtraction: ");
    oper.printMatrix(subtractMatrix);
    int[][] multiplyMatrix = oper.multiply(matrixOne, matrixTwo);
    System.out.println("Multiply: ");
    oper.printMatrix(multiplyMatrix);
  }
}
