package numberTwo;

public class Main {
  public static void main(String[] args) {
    Vectors vetOne = new Vectors(1, 9, 12, 4);
    Vectors vetTwo = new Vectors(2, 6, 7, 11);
    Vectors vetThree = new Vectors(10, 6, 5, 3);
    System.out.println(vetOne.toString());
    System.out.println(vetTwo.toString());
    System.out.println(Vectors.still(vetOne, vetTwo));
    System.out.println(Vectors.subtract(vetOne, vetTwo));
    System.out.println(vetThree.multiplySkalar(2));
    System.out.println(vetThree.Skalar(5));
  }
}
