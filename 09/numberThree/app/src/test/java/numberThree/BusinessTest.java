package numberThree;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class BusinessTest {
  private BusinessMan manOne;
  private BusinessMan manTwo;
  @BeforeEach
  public void nameHim() {
    manOne = new BusinessMan("Jake", "BusinessMan", 250000);
    manTwo = new BusinessMan("Jakee", "BusinessMan", 250000);
  }
  @Test void checkSalary() {
    assertEquals(250000, manOne.getSalary());
  }
  @Test void checkWork() {
    assertEquals("i am BusinessMan", manOne.printBusiness());
  }
  @Test void equalsTest() {
    assertEquals(false, manOne.equals(manTwo));
  }
  @Test void hashCodeTest() {
    assertNotEquals(manTwo.hashCode(), manOne.hashCode());
  }
}
