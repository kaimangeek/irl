package numberThree;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class WorkerTest {
  private Worker workOne;
  private Worker workTwo;
  @BeforeEach
  public void nameHim() {
    workOne = new Worker("Alex", "Worker", 62500);
    workTwo = new Worker("Alexx", "Worker", 62500);
  }
  @Test void checkSalary() {
    assertEquals(62500, workOne.getSalary());
  }
  @Test void checkWork() {
    assertEquals("i am Worker", workOne.printWorker());
  }
  @Test void equalsTest() {
    assertEquals(false, workOne.equals(workTwo));
  }
  @Test void hashCodeTest() {
    assertNotEquals(workTwo.hashCode(), workOne.hashCode());
  }
}
