package numberThree;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class MechanicTest {
  private Mechanic mechOne;
  private Mechanic metTwo;
  @BeforeEach
  public void nameHim() {
    mechOne = new Mechanic("Max", "Mechanic", 31125);
    mechTwo = new Mechanic("Maxx", "Mechanic", 31125);
  }
  @Test void checkSalary() {
    assertEquals(31125, mechOne.getSalary());
  }
  @Test void checkWork() {
    assertEquals("i am Mechanic", mechOne.printMechanic());
  }
  @Test void equalsTest() {
    assertEquals(false, mechOne.equals(mechTwo));
  }
  @Test void hashCodeTest() {
    assertNotEquals(mechTwo.hashCode(), mechOne.hashCode());
  }
}
