package numberThree;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class DeveloperTest {
  private Developer devOne;
  private Developer devTwo;
  @BeforeEach
  public void nameHim() {
    devOne = new Developer("Mike", "Developer", 125000);
    devTwo = new Developer("Mikee", "Developer", 125000);
  }
  @Test void checkSalary() {
    assertEquals(125000, devOne.getSalary());
  }
  @Test void checkWork() {
    assertEquals("i am developer", devOne.printDeveloper());
  }
  @Test void equalsTest() {
    assertEquals(false, devOne.equals(devTwo));
  }
  @Test void hashCodeTest() {
    assertNotEquals(devTwo.hashCode(), devOne.hashCode());
  }
}
