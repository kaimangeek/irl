package numberThree;

public class Getters implements Cloneable {
  protected String name;
  protected String work;
  protected int salary;
  public Getters(String name, String work, int salary) {
    this.name = name;
    this.work = work;
    this.salary = salary;
  }
  public Object cloning() throws CloneNotSupportedException {
    return super.clone();
  }
  public boolean equals(Object e) {
    if (this == e) return true;
    if (e == null || getClass() != e.getClass()) return false;
    Getters eOne = (Getters) e;
    if (this.work != eOne.work) return false;
    if (this.salary != eOne.salary) return false;
    return this.name.equals(eOne.name);
  }
  public int hashCode() {
    return name.length() * 100 + work.length() * 10 + salary;
  }
  public String toString() {
    return "Hello, my name is" + this.name + "am working for " + this.work + " my salary is " + this.salary;
  }
  public String getName() {
    return name;
  }
  public String getWork() {
    return work;
  }
  public int getSalary() {
    return salary;
  }
  public void setName(String name) {
    this.name = name;
  }
  public void setWork(String work) {
    this.work = work;
  }
  public void setSalary(int salary) {
    this.salary = salary;
  }
}
