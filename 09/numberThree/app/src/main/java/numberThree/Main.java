package numberThree;

class Main {
  public static void main(String[] args) {
    Getters[] arrayOfWorkers = new Getters[4];
    arrayOfWorkers[0] = new BusinessMan("Jake", "BusinessMan", 250000);
    arrayOfWorkers[1] = new Developer("Mike", "Developer", 125000);
    arrayOfWorkers[2] = new Worker("Alex", "Worker", 62500);
    arrayOfWorkers[3] = new Mechanic("Max", "Mechanic", 31125);
    for (int i=0; i<arrayOfWorkers.length; i++) {
      System.out.println(arrayOfWorkers[i].toString());
    }
  }
}
