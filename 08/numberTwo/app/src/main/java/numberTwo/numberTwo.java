// нужна диаграмма классов

package numberTwo;

import java.util.Random;
import java.util.Date;


public class numberTwo {
  private int[] numbers;
  private long readyTime;
  private long maxTime;
  private int numb;
  private boolean decision;
  public numberTwo(int numb, int maxTime) {
    this.numb = numb;
    this.numbers = new int[this.numb];
    this.maxTime = maxTime;
  }
  private void makeNumb() {
    long startTime = new Date().getTime();
    for (int i = 0; i < this.numb; i++) {
      this.numbers[i] = new Random().nextInt(1000);
    }
    this.maxTime = new Date().getTime() - startTime;
  }
  public boolean checkDecision() {
    boolean tr = true;
    boolean fl = false;
    if (this.readyTime <= this.maxTime)
      return tr;
    else
      return fl;
  }
  public int[] getDecision() {
    if (this.checkDecision() == true) {
      return this.numbers;
    }
    else {
      return null;
    }
  }
  private String error() {
    return "Ошибка.";
  }
}
