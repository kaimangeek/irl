package numberSix;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

class OperTest {
  private int[][] secArray;
  private int[][] thrArray;
  @BeforeEach
  public void checking() {
    secArray = new int[][]{{1, 5, 9},
                           {7, 3, 2},
                           {2, 5, 4}
    };
    thrArray = new int[][] {{2, 7, 1},
                            {5, 3, 7},
                            {4, 2, 9}

    };
    @Test void checkMultiply() {
      int[][] matrixTest = new int[][] {{12, 54, 35},
                                        {122, 212, 356},
                                        {380, 392, 460}

      };
      assertArrayEquals(matrixTest, Operations.multiply(secArray, thrArray), "passed");
    }
  }
}
