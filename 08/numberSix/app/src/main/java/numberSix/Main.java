package numberSix;

import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter a quantity of matrix:  ");
    int quant = sc.nextInt();
    System.out.println("Enter a size of matrix: ");
    int size = sc.nextInt();
    numberSix[] array = new numberSix[quant];
    for (int i=0; i<quant; i++) {
      array[i] = new numberSix(size);
    }
    long startTime = System.currentTimeMillis();
    int[][] matrixx = array[0].getSize();
    for (int i=0; i<quant; i++) {
      matrixx = Operations.multiply(matrixx, array[i].getSize());
      long endTime = System.currentTimeMillis();
      long all = endTime - startTime;
      System.out.println(all);
    }
  }
}
