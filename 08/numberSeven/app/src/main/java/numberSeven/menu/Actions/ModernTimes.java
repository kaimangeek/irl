package menu.Actions;

public class ModernTimes extends ActionPat {
  String description = "Выберите эпоху развития: ";
  public ModernTimes(String description) {
    super(description);
  }
  public void act() {
    System.out.println("Новейшая история, по мнению одних," + "\n" +
    "охватывает период с 1789 года до окончания второй мировой войны 1939—1945 годов," + "\n" +
    "а по мнению других — с 1918 года по настоящее время.");
  }
}
