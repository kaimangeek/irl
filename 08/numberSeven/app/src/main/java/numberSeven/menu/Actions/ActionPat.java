package menu.Actions;

abstract public class ActionPat {
  String description;
  ActionPat(String description) {
    this.description = description;
  }
  abstract public void act();
  public String getDescription() {
    return this.description;
  }
}
