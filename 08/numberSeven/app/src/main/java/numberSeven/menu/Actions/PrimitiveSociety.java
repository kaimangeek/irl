package menu.Actions;

public class PrimitiveSociety extends ActionPat {
  String description = "Выберите эпоху развития: ";
  public PrimitiveSociety(String description) {
    super(description);
  }
  public void act() {
    System.out.println("История первобытного общества охватывает период с момента появления первобытного человека до образования первых государств в Азии и Африке.");
  }
}
