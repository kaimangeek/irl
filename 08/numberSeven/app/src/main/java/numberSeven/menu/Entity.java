package menu;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import menu.Actions.*;

public class Entity {
  ArrayList<Entity> children = new ArrayList<Entity>();
  Entity parent;
  ArrayList<ActionPat> actions = new ArrayList<ActionPat>();

  public void addAction(ActionPat action) {
    this.actions.add(action);
  }

  public void nextAction(Entity nextTime) {
    this.children.add(nextTime);
    nextTime.parent = this;
  }

  public void user() {
    System.out.println("Выберите пункт: ");
    if (this.parent == null) {
      System.out.println("0. Выход");
    }
    else {
      System.out.println("0. Назад");
    }
    int num = 1;
    for (int i=0; i<this.children.size(); i++) {
      System.out.println(num + ". " + "Следующий уровень");
      num++;
    }
    for (int i=0; i<this.actions.size(); i++) {
        System.out.println(num +". " +this.actions.get(i).getDescription());
        num++;
    }
    System.out.println();
  }

}
