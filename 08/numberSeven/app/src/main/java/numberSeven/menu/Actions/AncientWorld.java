package menu.Actions;

public class AncientWorld extends ActionPat  {
  String description = "Выберите эпоху развития: ";
  public AncientWorld(String description) {
    super(description);
  }
  public void act() {
    System.out.println("История древнего мира изучает существование древнейших цивилизаций" + "\n" +
    "(Древний Восток, Древняя Греция, Древний Рим) с момента возникновения до V века н.э");
  }
}
