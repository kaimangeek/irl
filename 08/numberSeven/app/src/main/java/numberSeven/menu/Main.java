package menu;

import java.util.Scanner;
import menu.Actions.*;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int userInput;

    Entity startGame = new Entity();
    Entity levelOne = new Entity();
    Entity levelOneP2 = new Entity();
    Entity levelTwo = new Entity();
    Entity levelTwoP2 = new Entity();

    ActionPat primitive = new PrimitiveSociety("Выбрать эту эпоху");

    startGame.nextAction(levelOne);
    startGame.nextAction(levelOneP2);
    startGame.addAction(primitive);

    levelOne.addAction(new AncientWorld("Выбрать эту эпоху"));
    levelOne.nextAction(levelTwo);
    levelOne.nextAction(levelTwoP2);

    levelOneP2.addAction(new MiddleAges("Выбрать эту эпоху"));

    levelTwo.addAction(new NewTime("Выбрать эту эпоху"));

    levelTwoP2.addAction(new ModernTimes("Выбрать эту эпоху"));
    while (true) {
      startGame.user();
      userInput = sc.nextInt();
      System.out.println();
      if (userInput == 0) {
        if (startGame.parent == null) {
          System.out.println("Теперь вы знаете чуть больше!");
          break;
        }
        else startGame = startGame.parent;
      }
      if (userInput == 3) {
        startGame.actions.get(userInput - 3).act();
      }
      if (userInput == 1 && startGame.children.size() == 0) {
        startGame.actions.get(userInput - 1).act() ;
      }
      if (userInput > 0 && userInput < startGame.actions.size() + startGame.children.size()) {
        startGame = startGame.children.get(userInput - startGame.children.size() + 1);
      }
    }
  }
}
