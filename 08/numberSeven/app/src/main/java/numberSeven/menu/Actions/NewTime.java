package menu.Actions;

public class NewTime extends ActionPat {
  String description = "Выберите эпоху развития: ";
  public NewTime(String description) {
    super(description);
  }
  public void act() {
    System.out.println("Новой историей учёные считают XVI – конец XVIII века." + "\n" +
    "Хронологическим рубежом, отделяющим Новое время от последующей эпохи," + "\n" +
    "одни учёные считают начало Великой французской революции 1789 – 1799 годов," + "\n" +
    "другие – окончание первой мировой войны 1914 – 1918 годов.");
  }
}
