import java.util.Scanner;

public class NumberFour {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    System.out.print("Введите желаемый порог: ");
    double limit = sc.nextDouble();

    System.out.print("Введите желаемый коэфициент: ");
    double coef = sc.nextDouble();

    System.out.print("Введите желаемое время максимальной обработки: ");
    double maxTime = sc.nextDouble();

    long startTime = System.currentTimeMillis();

    double firstSit = -coef*limit;
    double secondSit = coef*limit;

    while (true) {
      Thread.sleep((int) (Math.random()*1000));
      double randomNumber = firstSit + (int) (Math.random() * secondSit);
      if (randomNumber > limit) {
        System.out.print("Число превысило заданный порог");
      }
    }
  }
}
