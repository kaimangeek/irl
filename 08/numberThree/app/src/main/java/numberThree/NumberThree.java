// нужна диаграмма классов

package numberthree;

public class NumberThree {
  public static void sort(double[] array) {
    int i = 0;
    int lenOf = array.length;
    double perm = 0;
    while (i != (lenOf - 1)) {
      if  (array[i] > array[i + 1]) {
        perm = array[i + 1];
        array[i + 1] = array[i];
        array[i] = perm;
        i = 0;
      }
      else {
        i = i + 1;
      }
    }
    for (i = 0; i < lenOf; i++) {
      System.out.print(array[i] + ", ");
    }
    System.out.println();
  }

  public static double[] createSortedArray(double[] arr) {
    double[] secArray = arr.clone();
    int lenOf = secArray.length;
    double perm = 0;
    int i = 0;
    while (i != (lenOf - 1)) {
      if  (secArray[i] > secArray[i + 1]) {
        perm = secArray[i + 1];
        secArray[i + 1] = secArray[i];
        secArray[i] = perm;
        i = 0;
      }
      else {
        i = i + 1;
      }
    }
    for (i = 0; i < 5; i++) {
      System.out.print(secArray[i] + ", ");
    }
    return secArray;
  }
}
